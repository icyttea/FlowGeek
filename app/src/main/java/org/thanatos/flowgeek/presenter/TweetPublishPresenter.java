package org.thanatos.flowgeek.presenter;

import android.os.Bundle;

import org.thanatos.flowgeek.ui.activity.TweetPublishActivity;

import nucleus.presenter.RxPresenter;

/**
 * Created by thanatos on 2/29/16.
 */
public class TweetPublishPresenter extends RxPresenter<TweetPublishActivity>{


    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
    }
}
